/****************** CODE CREER PAR NATHAN ROCHER **********
 * 
 * DATE : 24/11/2019
 * 
 * 
 * 
 *
 */

class Pipe {
	constructor(x, y, ecart) {
		this.x = x
		this.y = y
		this.ecart = ecart
	}

	Xg() {
		return this.x
	}

	Xd() {
		return this.x + IMAGE.pipetop.width
	}

	Y1h() {
		return this.y
	}

	Y1b() {
		return this.y + IMAGE.pipetop.height
	}

	Y2h() {
		return this.y + this.ecart
	}

	Y2b() {
		return this.y + this.ecart + IMAGE.pipetop.height
	}

	is_out_of_screen() {
		return (this.Xg() + IMAGE.pipetop.width) < 0
	}
}

class Bird {
	x = 80
	y = 150
	r = 0
	v = 0
	alive = true

	Xg() {
		return this.x
	}

	Xd() {
		return this.x + IMAGE.bird.width
	}

	Yh() {
		return this.y
	}

	Yb() {
		return this.y + IMAGE.bird.height
	}

	get_score() {
		return this.score || 0
	}

	set_score(score) {
		this.score = score
	}
}

const GameConfig = {
	gravity: 0.25,
	speed: 1,
	birdRotationSpeed: 4,
	max_fall_speed: 9.81,
	vitesse_pipe: 2,
	hauteur_saut: -5,

	background_vitesse: 0.5,
	base_vitesse: 2,

	canvas: {
		height: 512,
		width: document.documentElement.clientWidth * 0.9
	}
}

class GameState {

	background_position = 0
	base_position = 0

	Birds = []
	Pipes = []

	constructor(Brains, generation) {
		this.Start = Date.now()
		this.Brains = Brains
		this.generation = generation

		for (let i = 0; i < Brains.population.length; i++) {
			let bird = new Bird()
			this.Birds.push(bird)
		}
	}

	nextState() {

		this.background_position = (this.background_position - GameConfig.background_vitesse) % IMAGE.background.width
		this.base_position = (this.base_position - GameConfig.base_vitesse) % IMAGE.base.width

		for (let i = 0; i < this.Birds.length; i++) {
			if (this.Birds[i].alive) {
				if (this.Birds[i].v < GameConfig.max_fall_speed) {
					this.Birds[i].v = this.Birds[i].v + GameConfig.gravity
				}

				this.Birds[i].y += this.Birds[i].v

				if (this.Birds[i].v < 0) {
					this.Birds[i].r = -15
				} else if (this.Birds[i].r < 60) {
					this.Birds[i].r += GameConfig.birdRotationSpeed
				}

				let nextPipe = this.Pipes.find(pipe => this.Birds[i].Xd() < pipe.Xg() ) 

				let pipeCoord = nextPipe ? [nextPipe.Y1h(), nextPipe.Y2h(), nextPipe.Xg() - this.Birds[i].Xd() ] : [10, GameConfig.canvas.height - IMAGE.base.height, 500]

				let retourReseau = this.Brains.population[i].network.compute([ this.Birds[i].Yh(), this.Birds[i].Yb(), ...pipeCoord ])

				if (retourReseau > 0.5) {
					this.Birds[i].v = GameConfig.hauteur_saut
				}
			}
		}

		// VERIFICATION DES PIPES
		for (let pipe in this.Pipes) {

			// SI UN PIPE EST HORS DE LECRAN
			if (this.Pipes[pipe].is_out_of_screen()) {

				// ON LE SUPPRIME DU TABLEAU
				this.Pipes.shift()

				// ON AJOUTE ALORS UN NOUVEAU PIPE
				this.add_pipe()

				//SI IL EST TOUJOURS DANS L'ECRAN
			} else {

				//ON LE DEPLACE VERS LA GAUCHE
				this.Pipes[pipe].x = this.Pipes[pipe].x - GameConfig.vitesse_pipe

			}
		}
	}

	add_pipe() {
		this.Pipes.push(new Pipe(GameConfig.canvas.width, random(50, 200), random(120, 200)))
	}

	score() {
		return (Date.now() - this.Start) / 100
	}

	is_over() {

		let deadBirds = 0

		for (let i = 0; i < this.Birds.length; i++) {

			if (this.Birds[i].alive) {

				let MORT_PIPE = false

				for (let pipe of this.Pipes) {

					let DANS_LE_PIPE = this.Birds[i].Xd() > pipe.Xg() && this.Birds[i].Xg() < pipe.Xd()
					let HORS_ECART = this.Birds[i].Yh() > pipe.Y1h() && pipe.Y2h() > this.Birds[i].Yb()

					MORT_PIPE = DANS_LE_PIPE && !HORS_ECART

					if (MORT_PIPE) break
				}

				let MORT_BASE = (this.Birds[i].Yh() < 0) || (this.Birds[i].Yb() > (GameConfig.canvas.height - IMAGE.base.height))

				let MORT = MORT_BASE || MORT_PIPE

				this.Birds[i].alive = !MORT

				if (MORT) this.Birds[i].set_score(this.score())
			}

			deadBirds += this.Birds[i].alive ? 0 : 1
		}

		return deadBirds === this.Birds.length
	}

	async draw(ctx, bird_vertical, ia_input) {

		// SUPPRESSION DE TOUT CONTENU
		ctx.clearRect(0, 0, GameConfig.canvas.width, GameConfig.canvas.height);

		// AFFICHAGE DU FOND
		let s = 0;
		while (s < GameConfig.canvas.width * 4 / 3) {
			ctx.drawImage(IMAGE.background, this.background_position + s, 0);
			s += IMAGE.background.width
		}

		for (let i = 0; i < this.Birds.length; i++) {
			if (this.Birds[i].alive) {

				// AFFICHAGE DE L'OISEAU
				ctx.save();
				ctx.translate(this.Birds[i].Xg() + IMAGE.bird.width / 2, this.Birds[i].Yh() + IMAGE.bird.width / 2);
				ctx.rotate(this.Birds[i].r * Math.PI / 180);
				ctx.drawImage(IMAGE.bird, -IMAGE.bird.width / 2, -IMAGE.bird.width / 2);
				ctx.restore();

				// AFFICHAGE DES DONNEES Y SUR ET SOUS L'OISEAU
				if (bird_vertical) {
					ctx.beginPath();
					ctx.moveTo(this.Birds[i].Xg(), this.Birds[i].Yh());
					ctx.lineTo(this.Birds[i].Xd(), this.Birds[i].Yh());
					ctx.stroke();

					ctx.beginPath();
					ctx.moveTo(this.Birds[i].Xg(), this.Birds[i].Yb());
					ctx.lineTo(this.Birds[i].Xd(), this.Birds[i].Yb());
					ctx.stroke();

					ctx.font = '12px Flapp';
					ctx.fillText('Y : ' + Math.floor(this.Birds[i].Yh()), 80, this.Birds[i].Yh() - 5);
					ctx.strokeText('Y : ' + Math.floor(this.Birds[i].Yh()), 80, this.Birds[i].Yh() - 5);

					ctx.fillText('YH : ' + Math.floor(this.Birds[i].Yb()), 73, this.Birds[i].Yb() + 15);
					ctx.strokeText('YH : ' + Math.floor(this.Birds[i].Yb()), 73, this.Birds[i].Yb() + 15);
				}

			}
		}

		// AFFICHAGE DES PIPES
		for (let pipe of this.Pipes) {
			ctx.drawImage(IMAGE.pipetop, pipe.Xg(), (-IMAGE.pipetop.height) + pipe.Y1h());
			ctx.drawImage(IMAGE.pipebottom, pipe.Xg(), pipe.Y2h());
		}

		// AFFICHAGE DE LA BASE
		let b = 0;
		while (b < GameConfig.canvas.width * 4 / 3) {
			ctx.drawImage(IMAGE.base, this.base_position + b, GameConfig.canvas.height - IMAGE.base.height);
			b += IMAGE.base.width
		}

		// AFFICHAGE DU SCORE
		ctx.font = '20px Flapp';
		ctx.fillText('Score : ' + Math.floor(this.score()), 15, 30);
		ctx.strokeText('Score : ' + Math.floor(this.score()), 15, 30);

		ctx.fillText('Generation : ' + this.generation, 15, 60);
		ctx.strokeText('Generation : ' + this.generation, 15, 60);

		let alive = this.Birds.map((bird) => bird.alive)
		alive = alive.reduce((total, current) => total + current)

		ctx.fillText('En vie : ' +  alive + "/" + this.Birds.length, 15, 90);
		ctx.strokeText('En vie : ' + alive  + "/" + this.Birds.length, 15, 90);

	}

}

var IMAGE = {
	background: "img/background.png",
	bird: "img/bird.png",
	pipebottom: "img/pipebottom.png",
	pipetop: "img/pipetop.png",
	base: "img/base.png"
}

const $ = document.querySelector.bind(document)
const canvas = $('#Game');
const GAME_PRESS_KEY = [32, 38]
const FPS = 60;
let now;
let then = Date.now();
let interval = 1000 / FPS;
let delta;

let SPEED = 0

let SPACE_DOWN = false;

let timer = null;
let gameState;

let FlappyEvolution = new Generation(50, 0.2, [5, [15], 1]);

let remaining_pipe_to_add;
let frame_between_pipe;
let remaining_frame;

function initialize_game() {
	remaining_pipe_to_add = random(3, 7)
	frame_between_pipe = random(150, 250)
	remaining_frame = frame_between_pipe
}

function graphicLoop() {
	cancelAnimationFrame(timer);
	timer = requestAnimationFrame(graphicLoop);
	now = Date.now();
	delta = now - then;
	if (delta > interval) {
		then = now - (delta % interval);
		gameState.draw(ctx, $("#BIRD_Y").checked, $("#IA_INPUT").checked)
	}
}


async function gameLoop() {

	/****************** BOUCLE DU JEUX ******************/

	if (gameState.is_over()) {

		console.log('[ALL BIRDS ARE DEAD]')
		
		for(let i = 0; i < gameState.Birds.length; i++){
			FlappyEvolution.population[i].score = gameState.Birds[i].score
		}

		FlappyEvolution.generateNextGeneration()
	
		gameState = new GameState( FlappyEvolution, gameState.generation + 1 )

		initialize_game()
	}

	remaining_frame -= 1
	if (remaining_frame < 0 && remaining_pipe_to_add > 0) {
		remaining_frame = frame_between_pipe
		remaining_pipe_to_add -= 1
		gameState.add_pipe()
	}

	gameState.nextState()

	setTimeout(gameLoop, SPEED);

	/****************** BOUCLE DU JEUX ******************/
}


window.onload = async _ => {
	ctx = canvas.getContext('2d');
	ctx.canvas.width = document.documentElement.clientWidth * 0.9;
	ctx.fillStyle = "white";

	for (var img of Object.keys(IMAGE)) {
		await loadImage(img)
	}

	gameState = new GameState(FlappyEvolution, 1)

	$('body').addEventListener('keyup', event => {
		SPACE_DOWN = GAME_PRESS_KEY.includes(event.keyCode) ? false : SPACE_DOWN
	});

	$('body').addEventListener('keydown', event => {
		SPACE_DOWN = GAME_PRESS_KEY.includes(event.keyCode) ? true : SPACE_DOWN
	});

	initialize_game()
	gameLoop()
	graphicLoop()
}

function loadImage(index) {
	return new Promise(resolve => {
		var img = new Image()
		img.onload = x => {
			IMAGE[index] = img
			resolve()
		}
		img.src = IMAGE[index]
	})
}

function random(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}