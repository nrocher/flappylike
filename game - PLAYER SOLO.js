class Pipe {
	constructor(x, y, ecart) {
		this.x = x
		this.y = y
		this.ecart = ecart
	}

	Xg() { return this.x }

	Xd() { return this.x + IMAGE.pipetop.width }

	Y1h() { return this.y }

	Y1b() { return this.y + IMAGE.pipetop.height }

	Y2h() { return this.y + this.ecart }

	Y2b() { return this.y + this.ecart + IMAGE.pipetop.height }

	is_out_of_screen() { return (this.Xg() + IMAGE.pipetop.width) < 0  }
}

class Bird extends Genome {
	x = 80
	y = 150
	r = 0
	v = 0

	constructor(brain, score){
		super(brain, score)
	}

	Xg() { return this.x }

	Xd() { return this.x + IMAGE.bird.width }

	Yh() { return this.y }

	Yb() { return this.y + IMAGE.bird.height }

	get_score(){ return this.score || 0 }

	set_score(score){ this.score = score }
}

const GameConfig = {
	gravity : 0.25,
	speed : 1,
	birdRotationSpeed : 4,
	max_fall_speed : 9.81,
	vitesse_pipe : 2,
	hauteur_saut :-5,

	background_vitesse : 0.5,
	base_vitesse : this.vitesse_pipe,

	canvas : {
		height: 512,
		width: document.documentElement.clientWidth * 0.9
	}
}

class GameState {

	background_position = 0
	base_position = 0


	constructor() {
		this.Bird = new Bird()
		this.Start = Date.now()
		this.Pipes = []
	}

	nextState() {

		this.background_position = (this.background_position - GameConfig.background_vitesse) % IMAGE.background.width
		this.base_position = (this.base_position - GameConfig.base_vitesse) % IMAGE.base.width

		if (this.Bird.v < GameConfig.max_fall_speed) {
			this.Bird.v = this.Bird.v + GameConfig.gravity
		}

		this.Bird.y += this.Bird.v

		if (this.Bird.v < 0) {
			this.Bird.r = -15
		} else if (this.Bird.r < 60) {
			this.Bird.r += GameConfig.birdRotationSpeed
		}

		// VERIFICATION DES PIPES
		for(let pipe in this.Pipes) {

			// SI UN PIPE EST HORS DE LECRAN
			if(this.Pipes[pipe].is_out_of_screen()){

				// ON LE SUPPRIME DU TABLEAU
				this.Pipes.shift()

				// ON AJOUTE ALORS UN NOUVEAU PIPE
				this.add_pipe()

			//SI IL EST TOUJOURS DANS L'ECRAN
			} else {

				//ON LE DEPLACE VERS LA GAUCHE
				this.Pipes[pipe].x = this.Pipes[pipe].x - GameConfig.vitesse_pipe

			}
		}
	}

	add_pipe() { 
		this.Pipes.push(new Pipe(GameConfig.canvas.width, random(50, 200), random(120, 200)))
	}

	score() { return (Date.now() - this.Start) / 100 }

	fly() { this.Bird.v = GameConfig.hauteur_saut }

	is_over() {
		
		let MORT_PIPE = false

		for(let pipe of this.Pipes){

			let DANS_LE_PIPE = this.Bird.Xd() > pipe.Xg() && this.Bird.Xg() < pipe.Xd()
			let HORS_ECART =  this.Bird.Yh() > pipe.Y1h() && pipe.Y2h() > this.Bird.Yb()
			
			MORT_PIPE = DANS_LE_PIPE && !HORS_ECART

			if(MORT_PIPE) break
		}

		let MORT_BASE = (this.Bird.Yb() > (GameConfig.canvas.height - IMAGE.base.height))

		let MORT = MORT_BASE || MORT_PIPE

		if( MORT ) this.Bird.set_score(this.score())

		return MORT
	}

	async draw(ctx, bird_vertical, ia_input) { 

		// SUPPRESSION DE TOUT CONTENU
		ctx.clearRect(0, 0, GameConfig.canvas.width, GameConfig.canvas.height);

		// AFFICHAGE DU FOND
		let s = 0;
		while(s < GameConfig.canvas.width*4/3){
			ctx.drawImage(IMAGE.background, this.background_position + s, 0);
			s += IMAGE.background.width
		}

		// AFFICHAGE DE L'OISEAU
		ctx.save();
		ctx.translate(this.Bird.Xg() + IMAGE.bird.width/2, this.Bird.Yh() + IMAGE.bird.width/2);
		ctx.rotate(this.Bird.r * Math.PI / 180);
		ctx.drawImage(IMAGE.bird, -IMAGE.bird.width/2, -IMAGE.bird.width/2);
		ctx.restore();

		// AFFICHAGE DES DONNEES Y SUR ET SOUS L'OISEAU
		if (bird_vertical) {
			ctx.beginPath();
			ctx.moveTo(this.Bird.Xg(), this.Bird.Yh());
			ctx.lineTo(this.Bird.Xd(), this.Bird.Yh());
			ctx.stroke();
	
			ctx.beginPath();
			ctx.moveTo(this.Bird.Xg(), this.Bird.Yb());
			ctx.lineTo(this.Bird.Xd(), this.Bird.Yb());
			ctx.stroke();
	
			ctx.font = '12px Flapp';
			ctx.fillText('Y : ' + Math.floor(this.Bird.Yh()), 80, this.Bird.Yh() - 5);
			ctx.strokeText('Y : ' + Math.floor(this.Bird.Yh()), 80, this.Bird.Yh() - 5);
	
			ctx.fillText('YH : ' + Math.floor(this.Bird.Yb()), 73, this.Bird.Yb() + 15);
			ctx.strokeText('YH : ' + Math.floor(this.Bird.Yb()), 73, this.Bird.Yb() + 15);
		}

		// AFFICHAGE DES PIPES
		for(let pipe of this.Pipes){
			ctx.drawImage(IMAGE.pipetop, pipe.Xg(), (-IMAGE.pipetop.height) + pipe.Y1h());
			ctx.drawImage(IMAGE.pipebottom, pipe.Xg(), pipe.Y2h());
		}

		// AFFICHAGE DE LA BASE
		let b = 0;
		while(b < GameConfig.canvas.width*4/3){
			ctx.drawImage(IMAGE.base, this.base_position + b, GameConfig.canvas.height - IMAGE.base.height);
			b += IMAGE.base.width
		}

		// AFFICHAGE DU SCORE
		ctx.font = '20px Flapp';
		ctx.fillText('Score : ' + Math.floor( this.score() ), 15, 30);
		ctx.strokeText('Score : ' + Math.floor( this.score() ), 15, 30);

	}

}

var IMAGE = {
	background: "img/background.png",
	bird: "img/bird.png",
	pipebottom: "img/pipebottom.png",
	pipetop: "img/pipetop.png",
	base: "img/base.png"
}

const $ = document.querySelector.bind(document)
const canvas = $('#Game');
const GAME_PRESS_KEY = [32, 38]
const FPS = 60;
let now;
let then = Date.now();
let interval = 1000 / FPS;
let delta;

SPEED = 15

let SPACE_DOWN = false;

let timer = null;
let gameState;

let remaining_pipe_to_add;
let frame_between_pipe;
let remaining_frame;

function initialize_game(){ 
	remaining_pipe_to_add = random(1, 5)
	frame_between_pipe = random(100, 150)
	remaining_frame = frame_between_pipe
}


function graphicLoop() {
	cancelAnimationFrame(timer);
	timer = requestAnimationFrame(graphicLoop);
	now = Date.now();
	delta = now - then;
	if (delta > interval) {
		then = now - (delta % interval);

		gameState.draw(ctx, $("#BIRD_Y").checked, $("#IA_INPUT").checked)

	}
}


function gameLoop() {
	setTimeout(gameLoop, SPEED);

	/****************** BOUCLE DU JEUX ******************/

	if(SPACE_DOWN){
		gameState.fly()
	}

	if(gameState.is_over()){
		//FIXME: alert("Votre score : " + gameState.score() + " pts")
		gameState = new GameState()
		initialize_game()
	}

	remaining_frame -= 1
	if(remaining_frame < 0 && remaining_pipe_to_add > 0) {
		remaining_frame = frame_between_pipe
		remaining_pipe_to_add -= 1
		gameState.add_pipe()
	}

	gameState.nextState()

	/****************** BOUCLE DU JEUX ******************/

	gameState.draw(ctx, $("#BIRD_Y").checked, $("#IA_INPUT").checked)
}


window.onload = async _ => {
	ctx = canvas.getContext('2d');
	ctx.canvas.width = document.documentElement.clientWidth * 0.9;
	ctx.fillStyle = "white";

	for (var img of Object.keys(IMAGE)) {
		await loadImage(img)
	}

	gameState = new GameState()

	$('body').addEventListener('keyup', event => {
		SPACE_DOWN = GAME_PRESS_KEY.includes(event.keyCode) ? false : SPACE_DOWN
	});

	$('body').addEventListener('keydown', event => {
		SPACE_DOWN = GAME_PRESS_KEY.includes(event.keyCode) ? true : SPACE_DOWN
	});

	initialize_game()
	gameLoop()
	graphicLoop()
}

function loadImage(index) {
	return new Promise(resolve => {
		var img = new Image()
		img.onload = x => {
			IMAGE[index] = img
			resolve()
		}
		img.src = IMAGE[index]
	})
}

function random(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}