var IMAGE = {
	background: "img/background.png",
	bird: "img/bird.png",
	pipebottom: "img/pipebottom.png",
	pipetop: "img/pipetop.png",
	base: "img/base.png"
}

const $ = document.querySelector.bind(document)
const canvas = $('#Game');

const FPS = 60;
var now;
var then = Date.now();
var interval = 1000 / FPS;
var delta;

var ctx;

var start = Date.now();
var speed = 1

var click = []
var pipe = []

var birdR = 0
var birdY = 150;
var gravity = .25;
var birdVelocity = 0;
var rotation = 4;
var max_fall_speed = 9

var START = false

var birdX = 80

var birdWidth2 = IMAGE.bird.width / 2, birdHeight2 = IMAGE.bird.height / 2

const time = _ => (Date.now() - start) / (1 / speed * 15)

canvas.onclick = _ => {
	birdVelocity = -5
}

async function render() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	var MORT_PIPE = false
	var BIRD_Y = $("#BIRD_Y").checked
	var IA_INPUT = $("#IA_INPUT").checked

	// AFFICHAGE DU FONDS
	var backPos = time() * 0.1 % canvas.width
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.drawImage(IMAGE.background, canvas.width - backPos, 0);
	ctx.drawImage(IMAGE.background, -backPos, 0);

	// AFFICHAGE DE L'OISEAU
	ctx.save();
	ctx.translate(birdX + birdWidth2, birdY + birdWidth2);
	ctx.rotate(birdR * Math.PI / 180);
	ctx.drawImage(IMAGE.bird, -birdWidth2, -birdWidth2);
	ctx.restore();

	var birdXW = Math.floor(birdX + IMAGE.bird.width)
	var birdYH = Math.floor(birdY + IMAGE.bird.height)

	if (BIRD_Y) {
		ctx.beginPath();
		ctx.moveTo(birdX, birdY);
		ctx.lineTo(birdXW, birdY);
		ctx.stroke();

		ctx.beginPath();
		ctx.moveTo(birdX, birdYH);
		ctx.lineTo(birdXW, birdYH);
		ctx.stroke();

		ctx.font = '12px Flapp';
		ctx.fillText('Y : ' + Math.floor(birdY), 80, birdY - 5);
		ctx.strokeText('Y : ' + Math.floor(birdY), 80, birdY - 5);

		ctx.fillText('YH : ' + Math.floor(birdYH), 73, birdYH + 15);
		ctx.strokeText('YH : ' + Math.floor(birdYH), 73, birdYH + 15);
	}

	var nextPipe = false

	// AFFICHAGE DES PIPES
	for (var p of pipe) {

		const x = canvas.width + p.time - time()

		const pipeXW = Math.floor(x + IMAGE.pipebottom.width)
		const pipeYG = Math.floor(p.y + p.gap)

		if (x > birdX && !nextPipe && IA_INPUT) {
			nextPipe = true;

			ctx.font = '12px Flapp';

			ctx.strokeStyle = "#FF0000";
			ctx.beginPath();
			ctx.moveTo(birdXW, birdYH > p.y ? birdY : birdYH);
			ctx.lineTo(birdXW, p.y);
			ctx.moveTo(birdXW, p.y);
			ctx.lineTo(x, p.y);
			ctx.stroke();

			ctx.save();
			ctx.translate(birdXW - 5, birdY - 5);
			ctx.rotate(-90 * Math.PI / 180);
			ctx.fillText('Y : ' + Math.floor(birdY - p.y), 10, 0);
			ctx.strokeText('Y : ' + Math.floor(birdY - p.y), 10, 0);
			ctx.restore();

			ctx.strokeStyle = "#0000FF";
			ctx.beginPath();
			ctx.moveTo(birdXW, birdY > pipeYG ? birdY : birdYH);
			ctx.lineTo(birdXW, pipeYG);
			ctx.moveTo(birdXW, pipeYG);
			ctx.lineTo(x, pipeYG);
			ctx.stroke();

			ctx.save();
			ctx.translate(birdXW - 5, birdYH + 55);
			ctx.rotate(-90 * Math.PI / 180);
			ctx.fillText('Y : ' + Math.floor(pipeYG - birdYH), 10, 0);
			ctx.strokeText('Y : ' + Math.floor(pipeYG - birdYH), 10, 0);
			ctx.restore();

			ctx.fillText('Dist : ' + Math.floor(x - birdXW), birdXW + 5, p.y - 5);
			ctx.strokeText('Dist : ' + Math.floor(x - birdXW), birdXW + 5, p.y - 5);

			ctx.strokeStyle = "#000000";
			ctx.fillText('Dist : ' + Math.floor(x - birdXW), birdXW + 5, p.y - 5);
			ctx.strokeText('Dist : ' + Math.floor(x - birdXW), birdXW + 5, p.y - 5);
		}

		if ((x + IMAGE.pipetop.width) < 0) {
			pipe.shift()
		} else {
			ctx.drawImage(IMAGE.pipetop, x, (-IMAGE.pipetop.height) + p.y);
			ctx.drawImage(IMAGE.pipebottom, x, p.gap + p.y);
		}

		// SI LE PITIT OISEAU TOUCHE UN TUYEAU
		if (birdXW > x && birdX < pipeXW) {
			if (!(birdY > p.y && birdYH < pipeYG)) {
				MORT_PIPE = true
			}
		}
	}

	// AFFICHAGE DE LA BASE
	var basePos = time() % canvas.width
	ctx.drawImage(IMAGE.base, canvas.width - basePos, canvas.height - IMAGE.base.height);
	ctx.drawImage(IMAGE.base, -basePos, canvas.height - IMAGE.base.height);

	if (START) {
		// AFFICHAGE DU SCORE
		ctx.font = '20px Flapp';
		ctx.fillText('Score : ' + Math.floor(time()), 15, 30);
		ctx.strokeText('Score : ' + Math.floor(time()), 15, 30);
	} else {

	}

	const MORT_BASE = (birdY + IMAGE.bird.height / 2) > (canvas.height - IMAGE.base.height)

	// SI MORT DU PETIT OISEAU : OUIN OUIN ...
	if (MORT_BASE || MORT_PIPE) {
		START = false

		$("#StartGame").classList = ""

		alert("Votre score : " + Math.floor(time()) + " pts")

		birdY = 150
		start = Date.now()
		pipe = []
		click = []
		birdR = 0
	}

	if (birdVelocity < max_fall_speed) {
		birdVelocity = birdVelocity + gravity;
	}
	if (birdVelocity < 0) {
		birdR = -15;
	} else if (birdR < 60) {
		birdR += 4;
	}

	if (START == true) {
		birdY += birdVelocity
	} else {
		birdR = 0
		birdVelocity = 0
	}
}

async function addPipe() {
	if (START) {
		pipe.push({ time: time() + random(0, 50), gap: random(100, 150), y: random(100, canvas.height - 200) })
		setTimeout(addPipe, 2000 + random(0, 250))
	}
}

function random(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

function draw() {
	requestAnimationFrame(draw);
	now = Date.now();
	delta = now - then;
	if (delta > interval) {
		then = now - (delta % interval);
		render()
	}
}

function startGame() {
	start = Date.now()
	START = true

	$("#StartGame").classList = "hide"

	addPipe()
}

function loadImage(index) {
	return new Promise(resolve => {
		var img = new Image()
		img.onload = x => {
			IMAGE[index] = img
			resolve()
		}
		img.src = IMAGE[index]
	})
}

window.onload = async _ => {
	ctx = canvas.getContext('2d');

	ctx.fillStyle = "white";

	$("#StartGame").onclick = startGame

	for (var img of Object.keys(IMAGE)) {
		await loadImage(img)
	}

	birdWidth2 = IMAGE.bird.width / 2, birdHeight2 = IMAGE.bird.height / 2

	draw()
}