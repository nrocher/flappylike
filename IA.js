function sigmoid(val) {
	return (1 / (1 + Math.exp(-val)))
}

class Neuron {
	value = 0
	weights = []

	populate(size) {
		for (let i = 0; i < size; i++) {
			// On initialise les poids de chaque neuronne avec une valeur random de -1 à 1
			this.weights.push(Math.random() * 2 - 1)
		}
	}
}

class Layer {

	neurons = []

	constructor(id) {
		this.id = id
	}

	populate(NBRneuronne, POIDSneuronne) {
		for (let i = 0; i < NBRneuronne; i++) {
			let neuron = new Neuron()
			neuron.populate(POIDSneuronne)
			this.neurons.push(neuron)
		}
	}
}


class Network {

	layers = []

	// INPUTS = Nombre de neuronne en entrée
	// HIDDENS = Tableau de layer avec en valeur le nombre de neuronne pour un layer
	// OUTPUTS = Nombre de neuronne en sortie
	generateNetwork(inputs, hiddens, outputs) {

		let index = 0

		// LAYER D'INPUT
		let layer = new Layer(index)
		layer.populate(inputs, 0)
		this.layers.push(layer)

		index++

		let nbrOutputs = inputs

		// LAYER HIDDENS

		for (let nbrNeuronne of hiddens) {

			layer = new Layer(index)
			layer.populate(nbrNeuronne, nbrOutputs)
			this.layers.push(layer)

			nbrOutputs = nbrNeuronne

			index++
		}

		// LAYER D'OUTPUT
		layer = new Layer(index)
		layer.populate(outputs, nbrOutputs)
		this.layers.push(layer)
	}


	//DATA = entrée
	compute(data) {

		// VERIFICATION = Les données passé n'ont pas la même forme que l'entrée du réseau.
		if (data.length != this.layers[0].neurons.length) throw "Les données passé n'ont pas la même forme que l'entrée du réseau."

		// ON DEFINIT LES ENTREE DU PREMIER LAYER AU DONNEE QU'ON LUI PASSE
		for (let i in data) {
			this.layers[0].neurons[i].value = data[i]
		}

		let previousLayer = this.layers[0]

		for (let layerIndex = 1; layerIndex < this.layers.length; layerIndex++) {

			for (let idxNeuron in this.layers[layerIndex].neurons) {

				// On fait la somme de ...
				let sum = 0

				for (let idxPreviousNeuron in previousLayer.neurons) {

					// ... chaque valeur des neuronnes du layer précédent multiplier par le poids de notre neuronne actuel
					sum += previousLayer.neurons[idxPreviousNeuron].value * this.layers[layerIndex].neurons[idxNeuron].weights[idxPreviousNeuron]
				}

				// On calcule la valeur de notre neuronne actuelle en effectuant la fonction d'activiation sur la somme des neuronnes précédentes
				this.layers[layerIndex].neurons[idxNeuron].value = sigmoid(sum);

			}

			previousLayer = this.layers[layerIndex]

		}

		// On renvoit les valeurs des neuronnes du layer OUTPUTS 
		return this.layers[this.layers.length - 1].neurons.map(neuron => neuron.value)

	}

}

class Genome {

	constructor(network, score) {
		this.network = network
		this.score = score
	}

}

class Generation {

	population = []

	constructor(size, breedPourcentage, networkForm) {
		this.size = size
		this.breedPourcentage = breedPourcentage
		this.networkForm = networkForm

		for (let i = 0; i < size; i++) {
			let brain = new Network()
			brain.generateNetwork(networkForm[0], networkForm[1], networkForm[2])
			this.population.push(new Genome(brain, 0))
		}
	}

	breed() {

		let nbrBreedBirds = Math.floor(this.size * this.breedPourcentage)
		let ret = []

		for (let i = 0; i < nbrBreedBirds; i++) {

			let NetworkbaseOfTheNewGenome = Object.assign( Object.create( Object.getPrototypeOf(this.population[i].network)), this.population[i].network)
			let NewtworkOfaBadBird = this.population[0].network

			for (let i in NewtworkOfaBadBird.layers) {
				if (Math.random() <= 0.5) {
					NetworkbaseOfTheNewGenome.layers[i] = NewtworkOfaBadBird.layers[i];
				} else if (Math.random() > 0.7) {
					NetworkbaseOfTheNewGenome.layers[i] = NetworkbaseOfTheNewGenome.layers[i];
				}
			}

			ret.push(new Genome(NetworkbaseOfTheNewGenome, 0))

		}

		return ret
	}

	mutate() {
		let nbrBreedBirds = Math.floor(this.size * this.breedPourcentage)
		let nbrMutateBirds = this.size - nbrBreedBirds
		let ret = []

		for (let i = 0; i < nbrMutateBirds; i++) {

			let brain = new Network()
			brain.generateNetwork(this.networkForm[0], this.networkForm[1], this.networkForm[2])
			ret.push(new Genome(brain, 0))
			
		}

		return ret
	}

	generateNextGeneration() {

		if(this.population == []){

			for (let i = 0; i < this.size; i++) {
				let brain = new Network()
				brain.generateNetwork(this.networkForm[0], this.networkForm[1], this.networkForm[2])
				this.population[i] = new Genome(brain, 0)
			}

		} else {

			this.population = this.population.sort((g1, g2) => g2.score - g1.score )

			const newBreedPop  = this.breed()  // On reproduit les meilleurs
			const newMutatePop = this.mutate() // On change des valeurs pour essayer d'obtenir un meilleur résultat

			this.population = newBreedPop.concat(newMutatePop)
		}
	}
}